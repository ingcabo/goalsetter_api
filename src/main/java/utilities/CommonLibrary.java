package utilities;

import com.auth0.jwt.JWT;
import com.auth0.jwt.interfaces.Claim;
import com.auth0.jwt.interfaces.DecodedJWT;
import gherkin.deps.com.google.gson.Gson;
import io.restassured.response.Response;
import io.restassured.response.ResponseOptions;

import java.util.*;
import java.util.stream.Collectors;

public class CommonLibrary {
    private static boolean intiated = false;
    private static CommonLibrary commonLibrary;

    public static CommonLibrary getInstance() {

        if (!intiated) {
            commonLibrary = new CommonLibrary();
            return commonLibrary;
        } else {
            return commonLibrary;
        }
    }

    public String decodeJwtTokenToGet_field(String token, String field){
        DecodedJWT jwt = JWT.decode(token);
        Map<String, Claim> claims = jwt.getClaims();
        Claim rid = claims.get(field);
        return rid.asString();

    }

    public List<String> getTokensWithCollection(String str) {
        return Collections.list(new StringTokenizer(str, ",")).stream()
                .map(token -> (String) token)
                .collect(Collectors.toList());
    }

    public Map<String, Object> getResponseOrderByAsc(ResponseOptions<Response> response) {

        Map<String, Object> Obj = new Gson().fromJson(response.getBody().asString(), Map.class);

        LinkedHashMap<String, Object> sortedMap = new LinkedHashMap<>();
        Obj.entrySet()
                .stream()
                .sorted(Map.Entry.comparingByKey())
                .forEachOrdered(x -> sortedMap.put(x.getKey(), x.getValue()));
        return sortedMap;
    }
}
