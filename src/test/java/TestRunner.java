import com.cucumber.listener.Reporter;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.AfterClass;
import org.junit.runner.RunWith;
import java.io.File;

import static com.cucumber.listener.Reporter.loadXMLConfig;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = "src/test/java/features",
        glue = {"classpath:steps"},
        plugin = { "com.cucumber.listener.ExtentCucumberFormatter:src/test/report/cucumber-reports/report.html"},
        monochrome = true

)

public class TestRunner {
    @AfterClass
    public static void setup()
    {
        loadXMLConfig(new File("src/test/resources/configs/extension-config.xml"));
        Reporter.setSystemInfo("User Name", "AJ");
        Reporter.setSystemInfo("Application Name", "Test API ");
        Reporter.setSystemInfo("Operating System Type", "dsds");
        Reporter.setSystemInfo("Environment", "qa");
        Reporter.setTestRunnerOutput("Test Execution Cucumber Report");
    }
}