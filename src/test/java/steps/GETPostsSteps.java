package steps;

import baseTest.BaseTest;
import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import gherkin.deps.com.google.gson.Gson;
import io.restassured.response.Response;
import io.restassured.response.ResponseOptions;
import model.LoginBody;
import model.Posts;
import org.testng.annotations.Test;
import utilities.*;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class GETPostsSteps extends BaseTest {

    public static ResponseOptions<Response> response;
    public static String token;
    public static String rid;

    @Test
    @Given("^I perform authentication operation for \"([^\"]*)\" with body$")
    public void iPerformAuthenticationOperationForWithBody(String url, DataTable table) throws Throwable {

        var data = table.raw();
        LoginBody loginBody = new LoginBody();
        loginBody.setEmail(data.get(1).get(0));
        loginBody.setPassword(data.get(1).get(1));

        RestAssuredExtension RestAssuredV2 = new RestAssuredExtension(url, APIConstant.ApiMethods.POST, token);

        token = RestAssuredV2.Authenticate(loginBody);
    }

    @Test
    @Given("^I perform GET operation for \"([^\"]*)\" using rid from token$")
    public void iPerformGETOperationForUsingRidFromToken(String url) throws Throwable {
        rid = CommonLibrary.getInstance().decodeJwtTokenToGet_field(token,"rid");

        Map<String, String> queryParams = new HashMap<>();

        RestAssuredExtension RestAssuredV2 = new RestAssuredExtension(url+rid,APIConstant.ApiMethods.GET,token);
        response = RestAssuredV2.ExecuteWithQueryParams(queryParams);

        Map<String, Object> responseOrderAsc = CommonLibrary.getInstance().getResponseOrderByAsc(response);

        System.out.println("responseOrderAsc ==="+responseOrderAsc);
    }

    @Test
    @Then("^I should see the walletBalance equal to  \"([^\"]*)\" and financialStatus equal to \"([^\"]*)\"$")
    public void iShouldSeeTheWalletBalanceEqualToAndFinancialStatusEqualTo(String walletBalance, String financialStatus) throws Throwable {

        var post = response.getBody().as(Posts.class);

        assertThat(post.getRid(), equalTo(rid));
        assertThat(post.getWalletBalance(), equalTo(Double.parseDouble(walletBalance)));
        assertThat(post.getFinancialStatus(), equalTo(financialStatus));
    }

}
