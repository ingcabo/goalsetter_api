package model;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Posts {
    private String rid;
    private double walletBalance;
    private String financialStatus;
}
