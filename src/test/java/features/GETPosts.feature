@GETPosts
Feature: GETPosts
  Verify different GET operations using REST-assured

  @smoke
  Scenario: Verify GET operation with bearer authentication token
    Given I perform authentication operation for "/auth/credentials" with body
      | email                      | password  |
      | diego+auto03@goalsetter.co | Gs@12345 |
    Given I perform GET operation for "Parents/" using rid from token
    Then I should see the walletBalance equal to  "25.00" and financialStatus equal to "VIP"
