#  automation API

the **automation API** allows us to execute test on API mode using restAssured,Cucumber,

### Starting 🚀
These instructions will allow you to get a copy of the project running on your local machine for development and testing purposes.

See Deployment to know how to deploy the project.

### Prerequisites 📋
What things do you need to install the software and how to install them.

- [java 11](https://....).
- [Maven](https://...).


What things do you need to configure/run the test regression.

- install and activate the Open VPN.
- check or configure your device parameter in "inputs/"
- Open IntelliJIDEA. Click in “Open” search the path where this project Goalsetter_API was cloned and choose POM.xml and “Open” then Select “Open as Project ''.
- In this Project search the file src/test/java/TestRunner.java and click the right button and then click Run TestRunner.
- If everything is OK, the test will start its execution leaving the report in the following path src/test/report/cucumber-reports/


### Architecture description 📋

- environment variables -> inputs/
- utility classes -> src/main/java/utilities/
- features -> src/test/java/features/
- models -> src/test/java/model/
- steps -> src/test/java/steps/
- TestRunner -> src/test/java/TestRunner.java
